<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
/*
====================================================================================================
 Author: Aaron Waldon (Causing Effect)
 http://www.causingeffect.com
====================================================================================================
 This file must be placed in the /system/expressionengine/third_party/ce_benchmark folder in your ExpressionEngine installation.
 package 		CE Benchmark
 version 		Version 0.1
 copyright 		Copyright (c) 2010 Causing Effect, Aaron Waldon <aaron@causingeffect.com>
 Last Update	08 December 2010
----------------------------------------------------------------------------------------------------
 Purpose: For developers to benchmark anything with {exp:...} tags
====================================================================================================

License:
    * Do whatever you want with this code.
    * CE Benchmark is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
    * You assume all risk associated with the installation and use of CE Benchmark.
*/

$plugin_info = array(
	'pi_name' => 'CE Benchmark',
	'pi_version' => '0.1',
	'pi_author' => 'Aaron Waldon (Causing Effect)',
	'pi_author_url' => 'http://www.causingeffect.com/software/ee/ce_benchmark',
	'pi_description' => 'For developers to benchmark anything with {exp:...} tags',
	'pi_usage' => Ce_benchmark::usage()
);

class Ce_benchmark 
{
	function Ce_benchmark()
	{
		//EE super global
		$this->EE =& get_instance();
		
		//get the param
		$mark = $this->EE->TMPL->fetch_param('mark');
		
		if ( $mark != '')
		{
			$this->EE->benchmark->mark($mark);
			return;
		}
		return;
	}
	
	public function elapsed()
	{
		//EE super global
		$this->EE =& get_instance();
		
		//get the params
		$mark1 = $this->EE->TMPL->fetch_param('mark1');
		$mark2 = $this->EE->TMPL->fetch_param('mark2');
		
		//verify the data was passed in
		if ( $mark1 == '' || $mark2 == '' )
		{
			return $this->EE->TMPL->no_results();
		}
		
		$parsed = @$this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, array(
				'mark1' => $mark1,
				'mark2' => $mark2,
				'time' => $this->EE->benchmark->elapsed_time( $mark1, $mark2 )
				));
		if ($parsed === FALSE)
		{
			return $this->EE->TMPL->no_results();
		}
		return $parsed;
	}

	// This function describes how the plugin is used.
	public function usage() 
	{
		ob_start();
?>
This plugin allows developers to benchmark their plugins and modules (anything with a {exp:...} tag).  It is just a way to tap into CodeIgniters banchmarking class.  You can set multiple marks and then see the time between them.  For more info, see http://codeigniter.com/user_guide/libraries/benchmark.html

{!-- The below code marks the first point --}
{exp:ce_benchmark mark="ce_image_start"}

{!-- Now we run the plugin we want to benchmark --}
{exp:ce_img:make src="/images/example/cow.jpg" max_width="100" max_height="100"}<img src="{made}" alt="" width="{width}" height="{height}" />{/exp:ce_img:make}

{!-- Here we mark the second point --}
{exp:ce_benchmark mark="ce_image_end"}

{!-- Now that we have two benchmark points, we can get the time that elapsed between them --}
{exp:ce_benchmark:elapsed mark1="ce_image_start" mark2="ce_image_end"}<p>'{mark1}' to '{mark2}': {time} seconds</p>{/exp:ce_benchmark:elapsed}
<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	} /* End of usage() function */
	
} /* End of class */
/* End of file pi.ce_benchmark.php */ 
/* Location: ./system/expressionengine/third_party/ce_benchmark/pi.ce_benchmark.php */ 