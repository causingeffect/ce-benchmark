#CE Benchmark

##Description
For developers to benchmark anything with {exp:...} tags

##Installation
In your /system/expressionengine/third_party directory, create a folder named *ce_benchmark* and place the pi.ce_benchmark.php file in it.

##License
* Do whatever you want with this code.
* CE Benchmark is provided "as is" "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
* You assume all risk associated with the installation and use of CE Benchmark.

##Documentation
<http://www.causingeffect.com/software/ee/ce_benchmark>